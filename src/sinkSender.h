#ifndef _SINKSENDER_H_
#define _SINKSENDER_H_

#include <libconfig.h>


void sinkSenderInit(config_t *pCfg);
void sinkSenderPut(uint32_t seconds, uint32_t frequency);

#endif // _SINKSENDER_H_
