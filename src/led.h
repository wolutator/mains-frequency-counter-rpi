#ifndef _LED_H_
#define _LED_H_

#include <stdbool.h>
#include <libconfig.h>

typedef enum { E_RED, E_BLUE, E_GREEN } tColor;

void ledInit(config_t *pCfg);
void led(tColor color, bool state);

#endif // _LED_H_