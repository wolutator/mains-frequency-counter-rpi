#include <stdbool.h>
#include <wiringPi.h>
#include <libconfig.h>
#include <stdbool.h>
#include <strings.h>

#include "led.h"
#include "logging.h"


const char LED_KEY[] = "led";
const char DEFAULT_LED[] = "off";
const char *ledsActiveStr;
bool ledsActive;

const int GREEN_OUT = 20;
const int RED_OUT = 21;
const int BLUE_OUT = 26;

void ledInit(config_t *pCfg) {
  if (! config_lookup_string(pCfg, LED_KEY, &ledsActiveStr)) {
    ledsActiveStr = DEFAULT_LED;
  }
  ledsActive = (0 == strcasecmp(ledsActiveStr, "on")) || (0 == strcasecmp(ledsActiveStr, "true"));
  logmsg(LOG_INFO, "CONFIG: ledsActive=%d, ledsActiveStr=%s\n", ledsActive, ledsActiveStr);

  pinMode(GREEN_OUT, OUTPUT);
  digitalWrite(GREEN_OUT, 0);

  pinMode(RED_OUT, OUTPUT);
  digitalWrite(RED_OUT, 0);

  pinMode(BLUE_OUT, OUTPUT);
  digitalWrite(BLUE_OUT, 0);
}


void led (tColor color, bool state) {
  if (color == E_RED) {
    digitalWrite(RED_OUT, state ? 1 : 0);
  } else if (color == E_BLUE && ledsActive) {
    digitalWrite(BLUE_OUT, state ? 1 : 0);
  } else if (color == E_GREEN && ledsActive) {
    digitalWrite(GREEN_OUT, state ? 1 : 0);
  }
}
