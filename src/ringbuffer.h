#ifndef _RINGBUFFER_H_
#define _RINGBUFFER_H_

#include <stdint.h>

void ringbufferPut(uint32_t f);
uint32_t ringbufferGet();

#endif // _RINGBUFFER_H_