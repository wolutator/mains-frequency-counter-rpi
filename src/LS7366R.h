#ifndef _LS7366R_H_
#define _LS7366R_H_

#include <stdint.h>


void ls7366rInit(uint8_t spiChanIn);
uint32_t ls7366rReadOTR();


#endif // _LS7366R_H_
