#include <stdint.h>
#include <pthread.h>


#define BUFFER_SIZE 256

uint32_t buffer[BUFFER_SIZE+5];

uint32_t bufferReadIdx = 0;
uint32_t bufferWriteIdx = 0;

pthread_mutex_t eventMutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t eventSignal = PTHREAD_COND_INITIALIZER;


  

void ringbufferPut(uint32_t f) {
    if (bufferWriteIdx == (BUFFER_SIZE - 1)) {
        while (bufferReadIdx == BUFFER_SIZE);
    } else {
        while (bufferReadIdx == (bufferWriteIdx + 1));
    }

    buffer[bufferWriteIdx] = f;
    bufferWriteIdx++;

    if (bufferWriteIdx > BUFFER_SIZE) {
        bufferWriteIdx = 0;
    }

    pthread_mutex_lock(&eventMutex);
    pthread_cond_signal(&eventSignal);
    pthread_mutex_unlock(&eventMutex);
}


uint32_t ringbufferGet() {
    if (bufferReadIdx == bufferWriteIdx) {
        pthread_mutex_lock(&eventMutex);
        pthread_cond_wait(&eventSignal, &eventMutex);
        pthread_mutex_unlock(&eventMutex);
    }

    double res = buffer[bufferReadIdx];
    bufferReadIdx++;
    if (bufferReadIdx > BUFFER_SIZE) {
        bufferReadIdx = 0;
    }
    return res;
}


