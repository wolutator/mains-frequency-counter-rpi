#include <stdint.h>
#include <stdio.h>

void main() {
  uint8_t s = 25;
  uint8_t c = 0;
  uint8_t l = 0;
  uint8_t d = 0;

  for (uint8_t i = 0; i < 30; i++) {
    c += 25;
    d = c - l;
    printf("%d %d %d %d\n", i, c, l, d);
    l = c;
  }
}

